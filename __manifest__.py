{
    'name': 'Reporte stock minimo',
    'version': '14.0.1.0.0',
    'category': 'stock',
    'summery': 'Reporte stock minimo',
    'author': 'Fixdoo',
    'depends': ['base', 'stock', 'sale_management', 'purchase', 'product'],
    'data': [
        'security/ir.model.access.csv',
        'views/report_stock_minimo_views.xml',
    ],
    'license': "LGPL-3",
    'installable': True,
    'application': False,
}
