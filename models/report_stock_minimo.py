from odoo import api, fields, models, tools


class ReportStockMinimo(models.Model):
    _name = "report.stock.minimo"
    _auto = False
    _order = "product_id desc"

    product_id = fields.Many2one("product.product")
    referencia_interna = fields.Char("")
    pendiente_de_surtir = fields.Float()
    existencia_real = fields.Float()
    stock_minimo = fields.Float()
    stock_maximo = fields.Float()
    costo_sugerido = fields.Float(compute="_compute_ultima_compra", )
    proveedor_id = fields.Many2one("res.partner", compute="_compute_ultima_compra", )
    fecha_ultima_compra = fields.Datetime(compute="_compute_ultima_compra", )
    categoria_id = fields.Many2one("product.category", related="product_id.categ_id")
    compra_sugerida = fields.Float(string="Compra sugerida", compute="_compute_compra_sugerida")

    def _compute_compra_sugerida(self):
        for record in self:
            disponible = record.existencia_real - record.stock_maximo
            if disponible > 0:
                record.compra_sugerida = 0
            else:
                record.compra_sugerida = abs(disponible)

    def _compute_ultima_compra(self):
        for record in self:
            record.categoria_id = record.product_id.categ_id.id or None
            ultima_compra = record.env["purchase.order.line"].search([
                ("product_id", "=", record.product_id.id),
                ("state", "=", "purchase")
            ], order="date_order DESC", limit=1)
            if ultima_compra:
                record.costo_sugerido = ultima_compra.price_unit
                record.proveedor_id = ultima_compra.partner_id.id or None
                record.fecha_ultima_compra = ultima_compra.date_order or None
            else:
                record.costo_sugerido = 0.0
                record.proveedor_id = None
                record.fecha_ultima_compra = None

    def _get_main_request(self):
        request = """
            CREATE or REPLACE VIEW %s AS (
                WITH pendiente_surtir AS (
                    SELECT 
                        sum(product_uom_qty) AS x_Cantidad,
                        product_id AS x_producto
                    FROM stock_move
                    WHERE purchase_line_id is not null and state in ('assigned', 'waiting')
                    GROUP BY
                        product_id
                ),
                ultima_compra AS (
                    select  
                        DISTINCT ON (pol.product_id) pol.product_id AS x_producto,
                        pol.price_total AS x_Costo_sugerido,
                        max(pol.date_planned) AS x_Fecha,
                        po.partner_id AS X_proveedor
                    FROM purchase_order_line AS pol
                    INNER JOIN
                        purchase_order AS po
                    ON
                        pol.order_id = po.id
                    where pol.state in ('purchase')
                    GROUP BY
                        pol.product_id,
                        pol.price_total,
                        po.partner_id
                ),
                cantidad_disponible AS(
                    select 
                        sum(stock_quant.quantity) - sum(stock_quant.reserved_quantity) AS x_Existencia_real,
                        sum(stock_quant.reserved_quantity) AS x_stock_reservado,
                        stock_quant.product_id AS x_producto
                    from stock_quant AS stock_quant
                    inner join
                        stock_location AS stock_location
                    ON
                        stock_quant.location_id = stock_location.id
                    WHERE stock_location.usage = 'internal'
                    GROUP BY
                    stock_quant.product_id
                ),
                stock_min_max AS (
                    select 
                        sum(product_min_qty)/count(id)  AS x_stock_minimo,
                        sum(product_max_qty)/count(id) AS x_stock_maximo,
                        product_id AS x_producto,
                        count(id)
                    FROM stock_warehouse_orderpoint
                    GROUP BY
                        product_id,
                        product_min_qty,
                        product_max_qty
                ),
                producto AS (
                    SELECT 
                        id AS x_producto,
                        default_code AS x_referencia
                    FROM product_product
                )
                select
                    row_number() OVER (PARTITION BY true) as id,
                    producto.x_referencia AS referencia_interna,
                    producto.x_producto AS product_id,
                    cantidad_disponible.x_stock_reservado AS pendiente_de_surtir,
                    cantidad_disponible.x_Existencia_real AS existencia_real,
                    stock_min_max.x_stock_maximo AS stock_maximo,
                    stock_min_max.x_stock_minimo AS stock_minimo
                FROM
                    producto AS producto
                LEFT JOIN
                    cantidad_disponible AS cantidad_disponible
                ON
                    cantidad_disponible.x_producto = producto.x_producto
                left join 
                    stock_min_max AS stock_min_max
                ON
                    stock_min_max.x_producto = producto.x_producto
            )
        """ % (self._table,)
        return request

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute(self._get_main_request())
